package com.ingooo.chat.demo.model.po;

import lombok.Data;

/**
 * Create by 丶TheEnd on 2019/10/10 0010.
 * @author Administrator
 */
@Data
public class User {

    private int userId;
    private String userName;
    private String password;

    public User() {
    }

    public User(int userId, String userName, String password) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
    }
}
