package com.ingooo.chat.demo.model;

import lombok.Data;

/**
 * Create by 丶TheEnd on 2019/10/10 0010.
 * @author Administrator
 */
@Data
public class Msg {

    private int code;
    private String data;

    public Msg() {
    }

    public Msg(int code) {
        this.code = code;
    }

    public Msg(String data) {
        this.data = data;
    }

    public Msg(int code, String data) {
        this.code = code;
        this.data = data;
    }
}
