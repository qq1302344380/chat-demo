package com.ingooo.chat.demo.model.po;

import lombok.Data;

/**
 * Create by 丶TheEnd on 2019/10/11 0011.
 * @author Administrator
 */
@Data
public class Friend {

    private int friendId;
    private int thisUserId;
    private int otherUserId;

    public Friend() {
    }

    public Friend(int friendId, int thisUserId, int otherUserId) {
        this.friendId = friendId;
        this.thisUserId = thisUserId;
        this.otherUserId = otherUserId;
    }
}
