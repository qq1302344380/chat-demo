package com.ingooo.chat.demo.dao;

import com.ingooo.chat.demo.model.po.Friend;
import com.ingooo.chat.demo.model.po.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Create by 丶TheEnd on 2019/10/10 0010.
 * @author Administrator
 */
@Component
public class UserDao {

    private static List<User> userList = new LinkedList<>();
    private static List<Friend> friendList = new LinkedList<>();
    private static int USER_SIZE = 30;
    private static int FRIEND_SIZE = 100;

    /**
     * 初始化用户 和 好友关系
     */
    static {
        for (int i = 1; i <= USER_SIZE; i++) {
            userList.add(new User(i, i+"", i+""));
        }

        Random random = new Random();
        for (int i = 0; i < FRIEND_SIZE;) {
            int thisUserId = random.nextInt(30) + 1;
            int otherUserId = random.nextInt(30) + 1;
            if (thisUserId == otherUserId) {
                continue;
            }
            int small = thisUserId < otherUserId ? thisUserId : otherUserId;
            int big = thisUserId > otherUserId ? thisUserId : otherUserId;
            for (Friend f : friendList) {
                if (f.getThisUserId() == small) {
                    if (f.getOtherUserId() == big) {
                        continue;
                    }
                }
            }
            Friend friend = new Friend(i, small, big);
            friendList.add(friend);
            i++;
        }
        System.out.println(friendList.size());
    }

    /**
     * 通过 UserId 获取 User 对象
     * @param userId
     * @return
     */
    public User getUserById(int userId){
        for (User user : userList) {
            if (user.getUserId() == userId) {
                return user;
            }
        }
        return null;
    }

    /**
     * 通过 userName 获取 User 对象
     * @param userName
     * @return
     */
    public User getUserByUserName(String userName) {
        for (User user : userList) {
            if (user.getUserName().equals(userName)) {
                return user;
            }
        }
        return null;
    }

    /**
     * 获取当前用户的全部好友
     * @param userId
     * @return
     */
    public List<User> getFriends(int userId){
        List<User> userList = new ArrayList<>();
        for (Friend friend : friendList) {
            if (friend.getThisUserId() == userId) {
                User userById = getUserById(friend.getOtherUserId());
                userList.add(userById);
            } else if (friend.getOtherUserId() == userId) {
                User userById = getUserById(friend.getThisUserId());
                userList.add(userById);
            }
        }
        return userList;
    }
}
