package com.ingooo.chat.demo.config;

import com.alibaba.fastjson.JSON;
import com.ingooo.chat.demo.model.po.User;
import com.ingooo.websocket.util.handler.WsSocketHandle;
import com.ingooo.websocket.util.model.message.BasicMessage;
import com.ingooo.websocket.util.util.WsUtil;

import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Create by 丶TheEnd on 2019/10/10 0010.
 * @author Administrator
 */
public class WsHandler implements WsSocketHandle {

    public static Map<Integer, Session> userSessionMap = new ConcurrentHashMap<>(64);

    @Override
    public void onOpen(Session session, HttpSession httpSession) {
        User user = (User) httpSession.getAttribute("user");
        userSessionMap.put(user.getUserId(), session);
        WsUtil.sendMsgOnly(session, JSON.toJSONString(new BasicMessage("100", "LOGIN_OK", "登录成功")));
    }

    @Override
    public void onClose(Session session) {
        System.out.println("onClose");
    }

    @Override
    public void onMessage(String message, Session session) {
        System.out.println("onMessage");
    }

    @Override
    public void onError(Session session, Throwable error) {
        System.out.println("onError");
    }
}
