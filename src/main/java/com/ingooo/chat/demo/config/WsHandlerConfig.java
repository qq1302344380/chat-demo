package com.ingooo.chat.demo.config;

import com.ingooo.websocket.util.handler.WsSocketHandle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Create by 丶TheEnd on 2019/10/10 0010.
 * @author Administrator
 */
@Configuration
public class WsHandlerConfig {

    @Bean
    public WsSocketHandle getHandler(){
        return new WsHandler();
    }


}
