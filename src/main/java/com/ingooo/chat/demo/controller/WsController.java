package com.ingooo.chat.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Create by 丶TheEnd on 2019/10/10 0010.
 * @author Administrator
 */
@RestController
@RequestMapping("/ws")
public class WsController {

    @Autowired
    private HttpServletRequest request;

    @RequestMapping("/sessionId")
    public String sessionId(){
        return request.getSession().getId();
    }

}
