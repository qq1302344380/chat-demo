package com.ingooo.chat.demo.controller;

import com.ingooo.chat.demo.config.WsHandler;
import com.ingooo.chat.demo.dao.UserDao;
import com.ingooo.chat.demo.model.po.User;
import com.ingooo.websocket.util.model.message.BasicMessage;
import com.ingooo.websocket.util.model.message.SimpleChatMessage;
import com.ingooo.websocket.util.util.WsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;
import java.util.List;

/**
 * Create by 丶TheEnd on 2019/10/11 0011.
 * @author Administrator
 */
@RestController
@RequestMapping("/msg")
public class MsgController {

    @Autowired
    UserDao userDao;

    @Autowired
    HttpServletRequest request;

    @RequestMapping("/send/all")
    public void sendAll(@RequestParam String msg){
        int userId = ((User) request.getSession().getAttribute("user")).getUserId();
        List<User> userList = userDao.getFriends(userId);
        BasicMessage basicMessage = new BasicMessage("101", "SEND_MESSAGE", new SimpleChatMessage(userId + "", "all", msg, ""));
        WsUtil.sendMsgOnly(WsHandler.userSessionMap.get(userId), basicMessage);
        for (User user : userList) {
            Session session = WsHandler.userSessionMap.get(user.getUserId());
            if (session != null) {
                BasicMessage sendMessage = new BasicMessage("100", "RECEIVED_MESSAGE", new SimpleChatMessage(userId + "", "all", msg ,""));
                System.out.println("发送消息 --->> ");
                WsUtil.sendMsgOnly(session, sendMessage);
            }
        }
    }

    @RequestMapping("/send")
    public void sendOnly(@RequestParam int userId, String msg){

        int thisUserId = ((User) request.getSession().getAttribute("user")).getUserId();
        BasicMessage basicMessage = new BasicMessage("101", "SEND_MESSAGE", new SimpleChatMessage(userId + "", "only", msg, ""));
        WsUtil.sendMsgOnly(WsHandler.userSessionMap.get(thisUserId), basicMessage);
        Session session = WsHandler.userSessionMap.get(userId);
        BasicMessage sendMessage = new BasicMessage("100", "RECEIVED_MESSAGE", new SimpleChatMessage(thisUserId + "", "only", msg ,""));
        if (session != null) {
            System.out.println("发送消息 --->> ");
            WsUtil.sendMsgOnly(session, sendMessage);
        }
    }

}
