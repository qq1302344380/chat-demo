package com.ingooo.chat.demo.controller;

import com.ingooo.chat.demo.dao.UserDao;
import com.ingooo.chat.demo.model.Msg;
import com.ingooo.chat.demo.model.po.User;
import com.ingooo.websocket.util.model.message.BasicMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Create by 丶TheEnd on 2019/10/10 0010.
 * @author Administrator
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserDao userDao;

    @Autowired
    HttpServletRequest request;

    @RequestMapping("/login")
    public Msg login(User user){
        if (user.getUserName() == null || user.getPassword() == null) {
            return new Msg(400, "账号密码不能为空");
        }
        User byUserName = userDao.getUserByUserName(user.getUserName());
        if (byUserName != null && user.getPassword().equals(byUserName.getPassword())) {
            request.getSession().setAttribute("user", byUserName);
            return new Msg(1000, user.getUserName());
        } else {
            return new Msg(400, "账号密码错误");
        }
    }

    @RequestMapping("/friends")
    public BasicMessage getFriends(){
        User user = (User)request.getSession().getAttribute("user");
        if (user == null) {
            return new BasicMessage("400", "当前未登录", "");
        }
        List<User> friends = userDao.getFriends(user.getUserId());
        return new BasicMessage("100", "", friends);
    }

}
